/*
 * MIT License
 *
 * Copyright (c) 2022 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package eventsourced

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var tstamp time.Time

func init() {
	tstamp, _ = time.Parse("2006-01-02 15:04:05.999999999", "2019-08-07 10:32:44.123456789")
}

func TestHandleSingleCommandOnEmptyAggregate(t *testing.T) {
	store := NewInMemoryStore()
	publisher := &InMemoryPublisher{}
	handler, err := NewHandler(context.Background(), &Area{}, store,
		WithEventPublisher(publisher))
	assert.NoError(t, err)

	event, err := handler.Handle(context.Background(), &CreateArea{Name: "name"})
	assert.NoError(t, err)
	assert.NotNil(t, event)
	assert.IsType(t, &AreaCreated{}, event)
	areaCreated := event.(*AreaCreated)
	assert.True(t, len(areaCreated.ID) > 0)
	assert.NotNil(t, areaCreated.When())
	_, err = uuid.Parse(areaCreated.ID.String())
	assert.NoError(t, err)
	assert.Equal(t, "name", areaCreated.Name)

	assert.Equal(t, 1, len(store.roots[reflect.TypeOf(&Area{}).String()]))
	assert.Equal(t, 1, len(store.events))
	assert.Equal(t, 1, len(publisher.events))
}

func TestHandleSpecialIdGenerator(t *testing.T) {
	store := NewInMemoryStore()
	publisher := &InMemoryPublisher{}
	generator := func() string { return "123" }
	handler, err :=
		NewHandler(context.Background(), &Area{}, store,
			WithEventPublisher(publisher),
			WithIDGenerator(generator))
	assert.NoError(t, err)

	event, err := handler.Handle(context.Background(), &CreateArea{Name: "name"})
	assert.NoError(t, err)
	assert.NotNil(t, event)
	assert.IsType(t, &AreaCreated{}, event)
	areaCreated := event.(*AreaCreated)
	assert.Equal(t, ID("123"), areaCreated.ID)
}

func TestHandleErrorFetchingEvents(t *testing.T) {
	store := &InMemoryStore{GetError: errors.New("unable to fetch events")}
	publisher := &InMemoryPublisher{}
	id := ID("123")
	_, err :=
		NewHandler(context.Background(), &Area{BaseAggregate: BaseAggregate{&id}}, store,
			WithEventPublisher(publisher))

	assert.EqualError(t, err, "unable to fetch events")
}

func TestHandleCommandValidationFail(t *testing.T) {
	store := NewInMemoryStore()
	publisher := &InMemoryPublisher{}
	handler, err :=
		NewHandler(context.Background(), &Area{}, store,
			WithEventPublisher(publisher))
	assert.NoError(t, err)

	ev, err := handler.Handle(context.Background(), &CreateArea{Name: "name"})
	assert.NoError(t, err)
	id := ev.AggregateIdentity()
	aggregate := &Area{BaseAggregate: BaseAggregate{&id}}
	_, err =
		NewHandler(context.Background(), aggregate, store,
			WithEventPublisher(publisher))
	assert.NoError(t, err)
	_, err = handler.Handle(context.Background(), &CreateArea{Name: "name"})
	assert.EqualError(t, err, fmt.Sprintf("id of aggregate is not empty: %s", id.String()))
}

func TestHandleMissingAggregate(t *testing.T) {
	store := NewInMemoryStore()
	publisher := &InMemoryPublisher{}
	id := ID("1234")
	_, err :=
		NewHandler(context.Background(), &Area{BaseAggregate: BaseAggregate{&id}}, store,
			WithEventPublisher(publisher))
	assert.EqualError(t, err, "no aggregate with provided id and type was found")
}

func TestHandleApplyEvents(t *testing.T) {
	store := &InMemoryStore{
		events: map[string][]eventWrapper{
			"1234": {
				{
					id: 1,
					event: &AreaCreated{
						EventAggregateId: NewEventAggregateId("1234"),
						Name:             "name",
					},
				},
			},
		},
		roots: map[string][]ID{},
	}
	publisher := &InMemoryPublisher{}
	handler, err :=
		NewHandler(context.Background(), &Area{BaseAggregate: BaseAggregate{Id("1234")}}, store,
			WithEventPublisher(publisher))
	assert.NoError(t, err)

	event, err := handler.Handle(context.Background(), &RenameArea{NewName: "new name"})
	assert.NoError(t, err)
	assert.NotNil(t, event)
	assert.IsType(t, &AreaRenamed{}, event)
	areaRenamed := event.(*AreaRenamed)
	assert.Equal(t, ID("1234"), areaRenamed.AggregateIdentity())
	assert.Equal(t, "new name", areaRenamed.Name)
	assert.NotNil(t, areaRenamed.When())

	assert.Equal(t, 1, len(store.events))
	assert.Equal(t, 2, len(store.events["1234"]))
	assert.Equal(t, 1, len(publisher.events))
}

func TestHandleStaleAggregate(t *testing.T) {
	store := NewInMemoryStore()
	publisher := &InMemoryPublisher{}
	area := &Area{}
	handler, err := NewHandler(context.Background(), area, store, WithEventPublisher(publisher))
	assert.NoError(t, err)
	_, err = handler.Handle(context.Background(), &CreateArea{Name: "name"})
	assert.NoError(t, err)
	area2 := &Area{BaseAggregate: area.BaseAggregate}
	handler2, err :=
		NewHandler(context.Background(), area2,
			store,
			WithEventPublisher(publisher))
	assert.NoError(t, err)

	_, err = handler.Handle(context.Background(), &RenameArea{NewName: "new name"})
	assert.NoError(t, err)

	_, err = handler2.Handle(context.Background(), &RenameArea{NewName: "other name"})
	assert.NoError(t, err)

	assert.Equal(t, "new name", area.Name)
	assert.Equal(t, "other name", area2.Name)
}

func TestHandleStaleAggregate_ErrorFetchingEvents(t *testing.T) {
	count := 0
	store := &mockStore{
		getEvents: func(ctx context.Context, aggregate Aggregate, sinceSnapshot Snapshot) ([]EventWrapper, error) {
			defer func() {
				count += 1
			}()
			if count == 0 {
				return []EventWrapper{
					{
						Event: &AreaCreated{
							EventAggregateId: NewEventAggregateId("a1"),
							Name:             "area",
						},
						AggregateSequenceNo: 1,
					},
				}, nil
			}
			return nil, errors.New("error")
		},
		storeEvent: func(ctx context.Context, aggregate Aggregate, event Event, sequenceNo int, newAggregate bool) error {
			return ErrStaleAggregate
		},
	}
	publisher := &InMemoryPublisher{}
	area := &Area{BaseAggregate: BaseAggregateFromString("a1")}
	handler, err := NewHandler(context.Background(), area, store, WithEventPublisher(publisher))
	assert.NoError(t, err)
	_, err = handler.Handle(context.Background(), &RenameArea{NewName: "new name"})
	assert.EqualError(t, err, "error")
}

func TestHandleStaleAggregate_NoMoreEvents(t *testing.T) {
	count := 0
	store := &mockStore{
		getEvents: func(ctx context.Context, aggregate Aggregate, sinceSnapshot Snapshot) ([]EventWrapper, error) {
			defer func() {
				count += 1
			}()
			if count == 0 {
				return []EventWrapper{
					{
						Event: &AreaCreated{
							EventAggregateId: NewEventAggregateId("a1"),
							Name:             "area",
						},
						AggregateSequenceNo: 1,
					},
				}, nil
			}
			return nil, nil
		},
		storeEvent: func(ctx context.Context, aggregate Aggregate, event Event, sequenceNo int, newAggregate bool) error {
			return ErrStaleAggregate
		},
	}
	publisher := &InMemoryPublisher{}
	area := &Area{BaseAggregate: BaseAggregateFromString("a1")}
	handler, err := NewHandler(context.Background(), area, store, WithEventPublisher(publisher))
	assert.NoError(t, err)
	_, err = handler.Handle(context.Background(), &RenameArea{NewName: "new name"})
	assert.EqualError(t, err, "no more events found")
}

func TestHandleStaleAggregate_DeleteMarker(t *testing.T) {
	count := 0
	store := &mockStore{
		getEvents: func(ctx context.Context, aggregate Aggregate, sinceSnapshot Snapshot) ([]EventWrapper, error) {
			defer func() {
				count += 1
			}()
			if count == 0 {
				assert.Nil(t, sinceSnapshot)
				return []EventWrapper{
					{
						Event: &AreaCreated{
							EventAggregateId: NewEventAggregateId("a1"),
							Name:             "area",
						},
						AggregateSequenceNo: 1,
					},
				}, nil
			}
			assert.NotNil(t, sinceSnapshot)
			assert.Equal(t, 1, sinceSnapshot.SequenceNo())
			return []EventWrapper{
				{
					Event: &AreaRemoved{
						EventAggregateId: NewEventAggregateId("a1"),
						Name:             "area",
					},
					AggregateSequenceNo: 2,
				},
			}, nil
		},
		storeEvent: func(ctx context.Context, aggregate Aggregate, event Event, sequenceNo int, newAggregate bool) error {
			return ErrStaleAggregate
		},
	}
	publisher := &InMemoryPublisher{}
	area := &Area{BaseAggregate: BaseAggregateFromString("a1")}
	handler, err := NewHandler(context.Background(), area, store, WithEventPublisher(publisher))
	assert.NoError(t, err)
	_, err = handler.Handle(context.Background(), &RenameArea{NewName: "new name"})
	assert.EqualError(t, err, "no aggregate with provided id and type was found")
}

func TestHandleStaleAggregate_ErrorApplyingNewEvent(t *testing.T) {
	count := 0
	store := &mockStore{
		getEvents: func(ctx context.Context, aggregate Aggregate, sinceSnapshot Snapshot) ([]EventWrapper, error) {
			defer func() {
				count += 1
			}()
			if count == 0 {
				assert.Nil(t, sinceSnapshot)
				return []EventWrapper{
					{
						Event: &AreaCreated{
							EventAggregateId: NewEventAggregateId("a1"),
							Name:             "area",
						},
						AggregateSequenceNo: 1,
					},
				}, nil
			}
			assert.NotNil(t, sinceSnapshot)
			assert.Equal(t, 1, sinceSnapshot.SequenceNo())
			return []EventWrapper{
				{
					Event: &UnexpectedEvent{
						EventAggregateId: NewEventAggregateId("a1"),
					},
					AggregateSequenceNo: 2,
				},
			}, nil
		},
		storeEvent: func(ctx context.Context, aggregate Aggregate, event Event, sequenceNo int, newAggregate bool) error {
			return ErrStaleAggregate
		},
	}
	publisher := &InMemoryPublisher{}
	area := &Area{BaseAggregate: BaseAggregateFromString("a1")}
	handler, err := NewHandler(context.Background(), area, store, WithEventPublisher(publisher))
	assert.NoError(t, err)
	_, err = handler.Handle(context.Background(), &RenameArea{NewName: "new name"})
	assert.EqualError(t, err, "unexpected event '*eventsourced.UnexpectedEvent' for Area with id: a1")
}

func TestHandleStaleAggregate_ErrorStoringSnapshot(t *testing.T) {
	count := 0
	store := &mockStore{
		getEvents: func(ctx context.Context, aggregate Aggregate, sinceSnapshot Snapshot) ([]EventWrapper, error) {
			defer func() {
				count += 1
			}()
			if count == 0 {
				assert.Nil(t, sinceSnapshot)
				return []EventWrapper{
					{
						Event: &AreaCreated{
							EventAggregateId: NewEventAggregateId("a1"),
							Name:             "area",
						},
						AggregateSequenceNo: 1,
					},
				}, nil
			}
			assert.NotNil(t, sinceSnapshot)
			assert.Equal(t, 1, sinceSnapshot.SequenceNo())
			return []EventWrapper{
				{
					Event: &AreaRenamed{
						EventAggregateId: NewEventAggregateId("a1"),
						Name:             "new name",
					},
					AggregateSequenceNo: 2,
				},
			}, nil
		},
		storeEvent: func(ctx context.Context, aggregate Aggregate, event Event, sequenceNo int, newAggregate bool) error {
			if count == 1 {
				return ErrStaleAggregate
			}
			return nil
		},
		storeSnapshot: func(ctx context.Context, aggregate Aggregate, sequenceNo int) error {
			assert.Equal(t, 2, sequenceNo)
			return errors.New("error")
		},
	}
	publisher := &InMemoryPublisher{}
	area := &Area{BaseAggregate: BaseAggregateFromString("a1")}
	handler, err := NewHandler(
		context.Background(),
		area,
		store,
		WithEventPublisher(publisher),
		WithSnapshotInterval(2),
	)
	assert.NoError(t, err)
	_, err = handler.Handle(context.Background(), &RenameArea{NewName: "new name"})
	assert.NoError(t, err)
}

func TestHandleStaleAggregate_Success(t *testing.T) {
	count := 0
	store := &mockStore{
		getEvents: func(ctx context.Context, aggregate Aggregate, sinceSnapshot Snapshot) ([]EventWrapper, error) {
			defer func() {
				count += 1
			}()
			if count == 0 {
				assert.Nil(t, sinceSnapshot)
				return []EventWrapper{
					{
						Event: &AreaCreated{
							EventAggregateId: NewEventAggregateId("a1"),
							Name:             "area",
						},
						AggregateSequenceNo: 1,
					},
				}, nil
			}
			assert.NotNil(t, sinceSnapshot)
			assert.Equal(t, 1, sinceSnapshot.SequenceNo())
			return []EventWrapper{
				{
					Event: &AreaRenamed{
						EventAggregateId: NewEventAggregateId("a1"),
						Name:             "new name",
					},
					AggregateSequenceNo: 2,
				},
			}, nil
		},
		storeEvent: func(ctx context.Context, aggregate Aggregate, event Event, sequenceNo int, newAggregate bool) error {
			if count == 1 {
				return ErrStaleAggregate
			}
			return nil
		},
		storeSnapshot: func(ctx context.Context, aggregate Aggregate, sequenceNo int) error {
			assert.Equal(t, 2, sequenceNo)
			return nil
		},
	}
	publisher := &InMemoryPublisher{}
	area := &Area{BaseAggregate: BaseAggregateFromString("a1")}
	handler, err := NewHandler(
		context.Background(),
		area,
		store,
		WithEventPublisher(publisher),
		WithSnapshotInterval(2),
	)
	assert.NoError(t, err)
	_, err = handler.Handle(context.Background(), &RenameArea{NewName: "new name"})
	assert.NoError(t, err)
}

func TestEnrichBeforeSave(t *testing.T) {
	store := &mockStore{
		getEvents: func(ctx context.Context, aggregate Aggregate, sinceSnapshot Snapshot) ([]EventWrapper, error) {
			assert.Nil(t, sinceSnapshot)
			return []EventWrapper{
				{
					Event: &AreaCreated{
						EventAggregateId: NewEventAggregateId("a1"),
						Name:             "area",
					},
					AggregateSequenceNo: 1,
				},
			}, nil
		},
		storeEvent: func(ctx context.Context, aggregate Aggregate, event Event, sequenceNo int, newAggregate bool) error {
			event.SetWhen(time.Time{})
			assert.Equal(t, &AreaRenamed{Name: "new name", EventAggregateId: EventAggregateId{ID: "a1"}}, event)
			return nil
		},
		storeSnapshot: func(ctx context.Context, aggregate Aggregate, sequenceNo int) error {
			assert.Equal(t, 2, sequenceNo)
			return nil
		},
	}
	publisher := &InMemoryPublisher{}
	area := &Area{BaseAggregate: BaseAggregateFromString("a1")}
	handler, err := NewHandler(
		context.Background(),
		area,
		store,
		WithEventPublisher(publisher),
		WithSnapshotInterval(2),
	)
	assert.NoError(t, err)
	_, err = handler.Handle(context.Background(), &RenameArea{NewName: "new name"})
	assert.NoError(t, err)
}

func TestHandleApplyUnexpectedEvent(t *testing.T) {
	store := NewInMemoryStore()
	publisher := &InMemoryPublisher{}
	aggregate := &Area{}
	handler, err :=
		NewHandler(context.Background(), aggregate, store,
			WithEventPublisher(publisher))
	assert.NoError(t, err)

	_, err = handler.Handle(context.Background(), &UnexpectedCommand{})
	assert.EqualErrorf(t, err, err.Error(), "unexpected event '*eventsourced.UnexpectedEvent' for Area with id: %s", aggregate.ID.String())
}

func TestHandleApplyUnexpectedEventInEventLog(t *testing.T) {
	store := &InMemoryStore{
		events: map[string][]eventWrapper{
			"1234": {
				{
					id: 1,
					event: &UnexpectedEvent{
						EventAggregateId: NewEventAggregateId("1234"),
					},
				},
			},
		},
	}
	publisher := &InMemoryPublisher{}
	_, err :=
		NewHandler(context.Background(), &Area{BaseAggregate: BaseAggregate{Id("1234")}}, store,
			WithEventPublisher(publisher))
	assert.EqualError(t, err, "unexpected event '*eventsourced.UnexpectedEvent' for Area with id: 1234")
}

func TestHandleStoreError(t *testing.T) {
	store := NewInMemoryStore()
	store.StoreError = errors.New("unable to store event")

	publisher := &InMemoryPublisher{}
	handler, err :=
		NewHandler(context.Background(), &Area{}, store,
			WithEventPublisher(publisher))
	assert.NoError(t, err)

	_, err = handler.Handle(context.Background(), &CreateArea{Name: "name"})
	assert.EqualError(t, err, "unable to store event")
}

func TestHandlePublishError(t *testing.T) {
	store := NewInMemoryStore()
	publisher := &InMemoryPublisher{
		PublishError: errors.New("unable to publish event"),
	}
	handler, err :=
		NewHandler(context.Background(), &Area{}, store,
			WithEventPublisher(publisher))
	assert.NoError(t, err)

	_, err = handler.Handle(context.Background(), &CreateArea{Name: "name"})
	assert.EqualError(t, err, "unable to publish event")
}

func TestHandleSnapshotError(t *testing.T) {
	store := &InMemoryStore{GetSnapshotError: errors.New("unable to fetch snapshot")}
	publisher := &InMemoryPublisher{}
	id := ID("123")
	_, err :=
		NewHandler(context.Background(), &Area{BaseAggregate: BaseAggregate{&id}}, store,
			WithEventPublisher(publisher))

	assert.EqualError(t, err, "unable to fetch snapshot")
}

func TestHandleSnapshot(t *testing.T) {
	id := ID("123")

	store := &InMemoryStore{
		lastSnapshot: &snapshot{
			sequenceNo: 1,
			aggregate:  `{"ID":"123","Name":"Snapshot","Removed":false}`,
			when:       tstamp,
		},
	}
	publisher := &InMemoryPublisher{}

	aggregate := &Area{BaseAggregate: BaseAggregate{&id}}
	_, err :=
		NewHandler(context.Background(), aggregate, store,
			WithEventPublisher(publisher))

	assert.NoError(t, err)
	assert.NoError(t, err)
	assert.Equal(t, &id, aggregate.Identity())
	assert.Equal(t, "Snapshot", aggregate.Name)
	assert.False(t, aggregate.Removed)
}

func TestHandleSnapshotRemovedReCreation(t *testing.T) {

	store := NewInMemoryStore()

	aggregate := &Area{}
	h, err := NewHandler(context.Background(), aggregate, store, WithSnapshotInterval(1))
	_, _ = h.Handle(context.Background(), &CreateArea{Name: "name"})
	id := aggregate.Identity()
	assert.NoError(t, err)
	assert.NoError(t, err)

	store.lastSnapshot = nil
	aggregate = &Area{BaseAggregate: BaseAggregate{id}}
	_, err = NewHandler(context.Background(), aggregate, store, WithSnapshotInterval(1))
	assert.NoError(t, err)

	assert.NotNil(t, &id, store.lastSnapshot)
	assert.Equal(t, "name", aggregate.Name)
	assert.False(t, aggregate.Removed)
}

func TestHandleSnapshotCreation(t *testing.T) {
	store := NewInMemoryStore()
	publisher := &InMemoryPublisher{}
	aggregate := &Area{}
	handler, err :=
		NewHandler(context.Background(), aggregate, store,
			WithEventPublisher(publisher),
			WithSnapshotInterval(2),
			WithIDGenerator(func() string {
				return "abc-123"
			}))
	assert.NoError(t, err)

	_, err = handler.Handle(context.Background(), &CreateArea{Name: "name"})
	assert.NoError(t, err)
	assert.Nil(t, store.lastSnapshot)

	_, err = handler.Handle(context.Background(), &RenameArea{
		NewName: "new name",
	})
	assert.NoError(t, err)
	assert.Equal(t, &snapshot{
		sequenceNo: 2,
		aggregate:  `{"ID":"abc-123","Name":"new name","Closed":false,"Removed":false}`,
		when:       tstamp,
	}, store.lastSnapshot)
}

func TestHandleSnapshotCreationError(t *testing.T) {
	store := NewInMemoryStore()
	store.StoreSnapshotError = errors.New("unable to store snapshot")
	publisher := &InMemoryPublisher{}
	handler, err :=
		NewHandler(context.Background(), &Area{}, store,
			WithEventPublisher(publisher),
			WithSnapshotInterval(2))
	assert.NoError(t, err)

	_, err = handler.Handle(context.Background(), &CreateArea{Name: "name"})
	assert.NoError(t, err)
	assert.Nil(t, store.lastSnapshot)

	_, err = handler.Handle(context.Background(), &RenameArea{
		NewName: "new name",
	})
	assert.EqualError(t, err, "unable to store snapshot")
}

func TestHandleMultipleCommands(t *testing.T) {
	store := NewInMemoryStore()
	publisher := &InMemoryPublisher{}
	area := &Area{}
	processor, err :=
		NewHandler(context.Background(), area, store,
			WithEventPublisher(publisher))
	assert.NoError(t, err)

	event, err := processor.Handle(context.Background(), &CreateArea{Name: "Fluff"})
	assert.NoError(t, err)
	assert.NotNil(t, event)
	id := event.AggregateIdentity()
	assert.Equal(t, id, *area.ID)
	assert.Equal(t, "Fluff", area.Name)
	assert.Equal(t, false, area.Removed)

	_, err = processor.Handle(context.Background(), &RenameArea{
		NewName: "Blutti",
	})
	assert.NoError(t, err)
	assert.Equal(t, "Blutti", area.Name)
	assert.Equal(t, false, area.Removed)

	closedEvent, err := processor.Handle(context.Background(), &CloseArea{
		ID: id.String(),
	})
	assert.NoError(t, err)
	assert.Equal(t, "Blutti", area.Name)
	assert.Equal(t, true, area.Closed)
	assert.Equal(t, "Blutti", closedEvent.(*AreaClosed).Name)

	_, err = processor.Handle(context.Background(), &RenameArea{
		NewName: "Plutti",
	})
	assert.EqualError(t, err, fmt.Sprintf("area with id %s is closed and cannot be renamed", id))

	assert.Equal(t, 1, len(store.events))
	assert.Equal(t, 3, len(store.events[id.String()]))
	assert.Equal(t, 3, len(publisher.events))
}

func TestNoEventReturned(t *testing.T) {
	store := NewInMemoryStore()
	handler, err := NewHandler(context.Background(), &Area{}, store)
	assert.NoError(t, err)

	cmd := &NilEventCommand{}
	_, err = handler.Handle(context.Background(), cmd)
	assert.EqualError(t, err, "command returned nil event")
}

func TestMarkAsDeleted_Ok(t *testing.T) {
	store := NewInMemoryStore()
	aggregate := &Area{}
	h, err := NewHandler(context.Background(), aggregate, store, WithSnapshotInterval(1))
	require.NoError(t, err)
	_, _ = h.Handle(context.Background(), &CreateArea{})
	_, _ = h.Handle(context.Background(), &RenameArea{})
	_, _ = h.Handle(context.Background(), &RenameArea{})
	_, err = h.Handle(context.Background(), &RemoveArea{})
	require.NoError(t, err)
	_, err = NewHandler(context.Background(), aggregate, store)
	require.EqualError(t, err, ErrNoAggregate.Error())
}

func Test_HandleCommandAfterDelete(t *testing.T) {
	store := NewInMemoryStore()
	aggregate := &Area{}
	h, err := NewHandler(context.Background(), aggregate, store, WithSnapshotInterval(1))
	require.NoError(t, err)
	_, _ = h.Handle(context.Background(), &CreateArea{})
	_, _ = h.Handle(context.Background(), &RenameArea{})
	_, _ = h.Handle(context.Background(), &RenameArea{})
	_, _ = h.Handle(context.Background(), &RenameArea{})
	_, _ = h.Handle(context.Background(), DeleteAggregate{})
	require.NoError(t, err)
	_, err = h.Handle(context.Background(), &RenameArea{})
	require.EqualError(t, err, ErrNoAggregate.Error())
}

func Test_IllegalSnapshotInterval(t *testing.T) {
	store := NewInMemoryStore()
	aggregate := &Area{}
	_, err := NewHandler(context.Background(), aggregate, store, WithSnapshotInterval(0))
	require.ErrorContains(t, err, "snapshot interval must be larger at least 1")
}

type mockStore struct {
	getEvents     func(ctx context.Context, aggregate Aggregate, sinceSnapshot Snapshot) ([]EventWrapper, error)
	storeEvent    func(ctx context.Context, aggregate Aggregate, event Event, sequenceNo int, newAggregate bool) error
	storeSnapshot func(ctx context.Context, aggregate Aggregate, sequenceNo int) error
}

func (m mockStore) RestoreSnapshot(ctx context.Context, aggregate Aggregate) (Snapshot, error) {
	return nil, nil
}

func (m mockStore) StoreSnapshot(ctx context.Context, aggregate Aggregate, sequenceNo int) error {
	if m.storeSnapshot == nil {
		return nil
	}
	return m.storeSnapshot(ctx, aggregate, sequenceNo)
}

func (m mockStore) GetEvents(ctx context.Context, aggregate Aggregate, sinceSnapshot Snapshot) ([]EventWrapper, error) {
	if m.getEvents == nil {
		return nil, nil
	}
	return m.getEvents(ctx, aggregate, sinceSnapshot)
}

func (m mockStore) StoreEvent(ctx context.Context, aggregate Aggregate, event Event, sequenceNo int, newAggregate bool) error {
	if m.storeEvent == nil {
		return nil
	}
	return m.storeEvent(ctx, aggregate, event, sequenceNo, newAggregate)
}

func (m mockStore) GetAggregateRoots(ctx context.Context, aggregateType reflect.Type) ([]ID, error) {
	return nil, nil
}

func (m mockStore) Events(ctx context.Context, fromId int, c chan Event, aggregateTypes ...Aggregate) error {
	return nil
}

func (m mockStore) MaxGlobalSequenceNo(ctx context.Context) (int, error) {
	//TODO implement me
	panic("implement me")
}

var _ EventStore = &mockStore{}
