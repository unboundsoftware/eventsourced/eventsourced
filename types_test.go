/*
 * MIT License
 *
 * Copyright (c) 2023 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package eventsourced

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEventSequence_SetGlobalSequenceNo(t *testing.T) {
	type args struct {
		seqNo int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "success",
			args: args{seqNo: 1234},
			want: 1234,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := &EventGlobalSequence{}
			e.SetGlobalSequenceNo(tt.args.seqNo)
			assert.Equal(t, tt.want, e.GlobalSequenceNo())
		})
	}
}

func TestExternalEvent_Unwrap(t *testing.T) {
	type testCase[T Event] struct {
		name string
		e    ExternalEvent[T]
		want Event
	}
	tests := []testCase[*AreaCreated]{
		{
			name: "unwrapped",
			e: ExternalEvent[*AreaCreated]{
				WrappedEvent: &AreaCreated{
					Name: "Example Area",
				},
			},
			want: &AreaCreated{
				Name: "Example Area",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, tt.e.Unwrap(), "Unwrap()")
		})
	}
}

func TestExternalEvent_SetGlobalSequenceNo(t *testing.T) {
	type args struct {
		seqNo int
	}
	type testCase[T Event] struct {
		name string
		e    ExternalEvent[T]
		args args
	}
	tests := []testCase[*AreaCreated]{
		{
			name: "updates wrapped events global sequence no as well",
			e: ExternalEvent[*AreaCreated]{
				WrappedEvent: &AreaCreated{},
			},
			args: args{
				seqNo: 1234,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.e.SetGlobalSequenceNo(tt.args.seqNo)
			assert.Equal(t, tt.e.GlobalSequenceNo(), tt.args.seqNo)
			assert.Equal(t, tt.e.WrappedEvent.GlobalSequenceNo(), tt.args.seqNo)
		})
	}
}
