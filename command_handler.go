/*
 * MIT License
 *
 * Copyright (c) 2022 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package eventsourced

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"runtime"
	"time"

	"github.com/google/uuid"
)

// NewHandler return an initialized CommandHandler.
// Default values will be used where appropriate if not set explicitly using opts.
// Default value for idGenerator is UUID generator
// Default value for snapshotInterval is 10
func NewHandler(ctx context.Context, aggregate Aggregate, store EventStore, opts ...Option) (CommandHandler, error) {
	handler := &commandHandler{
		aggregate:        aggregate,
		store:            store,
		idGenerator:      uuidGenerator,
		publisher:        &noOpPublisher{},
		traceHandler:     noOpTraceHandler,
		snapshotInterval: 10,
	}
	for _, opt := range opts {
		if err := opt(handler); err != nil {
			return nil, fmt.Errorf("option function <%s> failed, %v", getOptionFuncName(opt), err)
		}
	}
	if err := handler.init(ctx); err != nil {
		return nil, err
	}
	return handler, nil
}

func (c *commandHandler) init(ctx context.Context) error {
	ctx, after := c.traceHandler.Trace(ctx, "eventsourced.init")
	defer after()
	id := c.aggregate.Identity()
	if id == nil {
		return nil
	}
	snapshot, err := c.store.RestoreSnapshot(ctx, c.aggregate)
	if err != nil {
		return err
	}
	events, err := c.store.GetEvents(ctx, c.aggregate, snapshot)
	if err != nil {
		return err
	}
	c.eventsSinceLastSnapshot = len(events)
	if len(events) == 0 && snapshot == nil {
		return ErrNoAggregate
	}

	if len(events) > 0 {
		lastWrapper := events[len(events)-1]
		if _, deleteMarker := lastWrapper.Event.(DeleteMarker); deleteMarker {
			return ErrNoAggregate
		}
		c.lastSequenceNo = lastWrapper.AggregateSequenceNo
	} else {
		c.lastSequenceNo = snapshot.SequenceNo()
	}
	for _, e := range events {
		if err := c.aggregate.Apply(e.Event); err != nil {
			return err
		}
	}

	if c.eventsSinceLastSnapshot >= c.snapshotInterval {
		// Potentially we can return err here instead, but the Aggregate is in fact ok,
		// so we just ignore it for now
		if err := c.store.StoreSnapshot(ctx, c.aggregate, events[len(events)-1].AggregateSequenceNo); err == nil {
			c.eventsSinceLastSnapshot = 0
		}
	}

	return nil
}

func (c *commandHandler) Handle(ctx context.Context, command Command) (Event, error) {
	ctx, after := c.traceHandler.Trace(ctx, "eventsourced.Handle")
	defer after()
	if c.isDeleted {
		return nil, ErrNoAggregate
	}
	if err := command.Validate(ctx, c.aggregate); err != nil {
		return nil, err
	}
	id, existing := getOrCreateID(c.aggregate.Identity(), c.idGenerator)
	event := command.Event(ctx)
	if event == nil {
		return nil, errors.New("command returned nil event")
	}
	event.SetWhen(time.Now())
	event.SetAggregateIdentity(id)
	if !existing {
		c.aggregate.SetIdentity(id)
	}
	_, c.isDeleted = event.(DeleteMarker)

	if !c.isDeleted {
		if err := func() error {
			_, after := c.traceHandler.Trace(ctx, "eventsourced.Apply")
			defer after()
			return c.aggregate.Apply(event)
		}(); err != nil {
			return nil, err
		}
	}

	if f, ok := event.(EnrichableEvent); ok {
		f.EnrichFromAggregate(c.aggregate)
	}

	sequenceNo := c.lastSequenceNo + 1
	err := c.store.StoreEvent(ctx, c.aggregate, event, sequenceNo, !existing)
	if err != nil {
		if errors.Is(err, ErrStaleAggregate) {
			if err := c.refresh(ctx); err != nil {
				return nil, err
			}
			return c.Handle(ctx, command)
		}
		return nil, err
	}
	c.lastSequenceNo = sequenceNo
	c.eventsSinceLastSnapshot = c.eventsSinceLastSnapshot + 1
	if c.eventsSinceLastSnapshot == c.snapshotInterval && !c.isDeleted {
		if err := c.store.StoreSnapshot(ctx, c.aggregate, sequenceNo); err != nil {
			return nil, err
		}
		c.eventsSinceLastSnapshot = 0
	}
	if err := c.publisher.Publish(ctx, event); err != nil {
		return nil, err
	}

	return event, nil
}

func (c *commandHandler) refresh(ctx context.Context) error {
	events, err := c.store.GetEvents(ctx, c.aggregate, refreshSnapshot{sequenceNo: c.lastSequenceNo})
	if err != nil {
		return err
	}
	c.eventsSinceLastSnapshot += len(events)
	if len(events) == 0 {
		return fmt.Errorf("no more events found")
	}

	lastWrapper := events[len(events)-1]
	if _, deleteMarker := lastWrapper.Event.(DeleteMarker); deleteMarker {
		return ErrNoAggregate
	}
	c.lastSequenceNo = lastWrapper.AggregateSequenceNo

	for _, e := range events {
		if err := c.aggregate.Apply(e.Event); err != nil {
			return err
		}
	}

	if c.eventsSinceLastSnapshot >= c.snapshotInterval {
		// Potentially we can return err here instead, but the Aggregate is in fact ok,
		// so we just ignore it for now
		if err := c.store.StoreSnapshot(ctx, c.aggregate, events[len(events)-1].AggregateSequenceNo); err == nil {
			c.eventsSinceLastSnapshot = 0
		}
	}
	return nil
}

type commandHandler struct {
	aggregate               Aggregate
	store                   EventStore
	publisher               EventPublisher
	idGenerator             IDGenerator
	traceHandler            TraceHandler
	snapshotInterval        int
	eventsSinceLastSnapshot int
	lastSequenceNo          int
	isDeleted               bool
}

type refreshSnapshot struct {
	sequenceNo int
}

func (r refreshSnapshot) SequenceNo() int {
	return r.sequenceNo
}

var _ Snapshot = refreshSnapshot{}

func getOrCreateID(id *ID, generator func() string) (ID, bool) {
	if id != nil {
		return *id, true
	}
	return ID(generator()), false
}

func uuidGenerator() string {
	return uuid.Must(uuid.NewV7()).String()
}

func getOptionFuncName(f Option) string {
	return runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name()
}
