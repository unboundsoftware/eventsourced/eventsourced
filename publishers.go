// Copyright (c) 2020 Unbound Software Development Svenska AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package eventsourced

import "context"

type channelPublisher struct {
	channels []chan Event
}

// NewChannelPublisher creates an EventPublisher which publishes events on the provided channels
func NewChannelPublisher(channels ...chan Event) EventPublisher {
	return &channelPublisher{channels: channels}
}

func (c channelPublisher) Publish(_ context.Context, event Event) error {
	for _, c := range c.channels {
		c <- event
	}
	return nil
}

var _ EventPublisher = &channelPublisher{}

type noOpPublisher struct{}

func (n noOpPublisher) Publish(context.Context, Event) error {
	return nil
}

func (n noOpPublisher) Stop(context.Context) error {
	return nil
}

var _ EventPublisher = &noOpPublisher{}
