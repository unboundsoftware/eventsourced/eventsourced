/*
 * MIT License
 *
 * Copyright (c) 2022 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package eventsourced

import "context"

// TraceHandler defines the required methods used for adding tracing capabilities
type TraceHandler interface {
	// Trace should add the provided name as a span.
	// The returned Context should be used in any further calls to enable span nesting.
	// The returned func will be called as a defer function.
	Trace(ctx context.Context, name string) (context.Context, func())
}

// The TraceHandlerFunc type is an adapter to allow the use of
// ordinary functions as trace handlers. If f is a function
// with the appropriate signature, TraceHandlerFunc(f) is a
// handler that calls f.
type TraceHandlerFunc func(ctx context.Context, name string) (context.Context, func())

// Trace implements the TraceHandler interface by calling the function itself.
func (t TraceHandlerFunc) Trace(ctx context.Context, name string) (context.Context, func()) {
	return t(ctx, name)
}

var noOpTraceHandler TraceHandler = TraceHandlerFunc(func(ctx context.Context, _ string) (context.Context, func()) {
	return ctx, func() {}
})
