package eventsourced

import (
	"time"
)

// NewEventAggregateId creates a new EventAggregateId
func NewEventAggregateId(id ID) EventAggregateId {
	return EventAggregateId{ID: id}
}

// EventAggregateId is a base struct that can be used by Events
type EventAggregateId struct {
	ID ID `json:"id"`
}

// AggregateIdentity implements parts of the Event interface
func (r *EventAggregateId) AggregateIdentity() ID {
	return r.ID
}

// SetAggregateIdentity implements parts of the Event interface
func (r *EventAggregateId) SetAggregateIdentity(id ID) {
	r.ID = id
}

// EventTime is a base struct that can be used by Events
type EventTime struct {
	Time time.Time `json:"time"`
}

// When implements parts of the Event interface
func (r *EventTime) When() time.Time {
	return r.Time
}

// SetWhen implements parts of the Event interface
func (r *EventTime) SetWhen(t time.Time) {
	r.Time = t
}

type EventGlobalSequence struct {
	SeqNo int `json:"sequenceNo"`
}

func (e *EventGlobalSequence) SetGlobalSequenceNo(seqNo int) {
	e.SeqNo = seqNo
}

func (e *EventGlobalSequence) GlobalSequenceNo() int {
	return e.SeqNo
}

// BaseAggregateFromString creates a new BaseAggregate using the id in the provided string
func BaseAggregateFromString(id string) BaseAggregate {
	return BaseAggregate{
		ID: IdFromString(id),
	}
}

// BaseAggregate is a base struct that can be used by Aggregates
type BaseAggregate struct {
	ID *ID
}

// Identity implements parts of the Aggregate interface
func (b *BaseAggregate) Identity() *ID {
	return b.ID
}

// SetIdentity implements parts of the Aggregate interface
func (b *BaseAggregate) SetIdentity(id ID) {
	b.ID = &id
}

// ExternalSource is a marker interface for events from external sources
type ExternalSource interface {
	ExternalSource() string
}

// ExternalEvent is events from an external source
type ExternalEvent[T Event] struct {
	BaseEvent
	WrappedEvent T      `json:"wrappedEvent"`
	Source       string `json:"source"`
	TypeName     string `json:"typeName"`
}

func (e *ExternalEvent[T]) Unwrap() Event {
	return e.WrappedEvent
}

func (e *ExternalEvent[T]) SetGlobalSequenceNo(seqNo int) {
	e.SeqNo = seqNo
	e.WrappedEvent.SetGlobalSequenceNo(seqNo)
}

var _ Event = &ExternalEvent[Event]{}
