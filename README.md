# eventsourced

[![GoReportCard](https://goreportcard.com/badge/gitlab.com/unboundsoftware/eventsourced/eventsourced)](https://goreportcard.com/report/gitlab.com/unboundsoftware/eventsourced/eventsourced) [![GoDoc](https://godoc.org/gitlab.com/unboundsoftware/eventsourced/eventsourced?status.svg)](https://godoc.org/gitlab.com/unboundsoftware/eventsourced/eventsourced) [![Build Status](https://gitlab.com/unboundsoftware/eventsourced/eventsourced/badges/main/pipeline.svg)](https://gitlab.com/unboundsoftware/eventsourced/eventsourced/commits/main)[![coverage report](https://gitlab.com/unboundsoftware/eventsourced/eventsourced/badges/main/coverage.svg)](https://gitlab.com/unboundsoftware/eventsourced/eventsourced/commits/main)

Package eventsourced provides the core of an eventsourced framework for Golang.

Download:
```shell
go get gitlab.com/unboundsoftware/eventsourced/eventsourced
```



