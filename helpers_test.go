// Copyright (c) 2020 Unbound Software Development Svenska AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package eventsourced

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"sort"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestEvents_Success(t *testing.T) {
	store := &InMemoryStore{events: map[string][]eventWrapper{
		"1234": {
			{
				id: 1,
				event: &AreaCreated{
					Name: "name",
				},
			},
			{
				id: 2,
				event: &AreaRenamed{
					Name: "updated",
				},
			},
		},
	}}

	var events []Event
	err :=
		Events(context.Background(), &Area{BaseAggregate: BaseAggregate{ID: Id("1234")}}, store, func(event Event, seqNo int) error {
			events = append(events, event)
			return nil
		})
	assert.NoError(t, err)

	assert.Equal(t, 2, len(events))
	assert.IsType(t, &AreaCreated{}, events[0])
	assert.IsType(t, &AreaRenamed{}, events[1])
}

func TestEvents_StoreError(t *testing.T) {
	store := &InMemoryStore{GetError: errors.New("error retrieving events")}

	var events []Event
	err :=
		Events(context.Background(), &Area{BaseAggregate: BaseAggregate{ID: Id("1234")}}, store, func(event Event, seqNo int) error {
			events = append(events, event)
			return nil
		})
	assert.EqualError(t, err, "error retrieving events")
}

func TestEvents_FuncError(t *testing.T) {
	store := &InMemoryStore{events: map[string][]eventWrapper{
		"1234": {
			{
				id: 1,
				event: &AreaCreated{
					Name: "name",
				},
			},
		},
	}}

	err :=
		Events(context.Background(), &Area{BaseAggregate: BaseAggregate{ID: Id("1234")}}, store, func(event Event, seqNo int) error {
			return errors.New("func error")
		})
	assert.EqualError(t, err, "func error")
}

func TestEventsFrom_Success(t *testing.T) {
	store := &InMemoryStore{events: map[string][]eventWrapper{
		"1234": {
			{
				id: 1,
				event: &AreaCreated{
					Name: "name",
				},
			},
			{
				id: 2,
				event: &AreaRenamed{
					Name: "updated",
				},
			},
		},
	}}

	var events []Event
	c := make(chan Event)
	done := make(chan bool)
	go func() {
		for event := range c {
			events = append(events, event)
		}
		done <- true
	}()
	err := EventsFrom(context.Background(), store, 0, c)
	assert.NoError(t, err)
	close(c)
	<-done
	assert.Equal(t, 2, len(events))
	assert.IsType(t, &AreaCreated{}, events[0])
	assert.IsType(t, &AreaRenamed{}, events[1])
}

func TestEventsFrom_StoreError(t *testing.T) {
	store := &InMemoryStore{GetError: errors.New("error retrieving events")}

	c := make(chan Event)
	err := EventsFrom(context.Background(), store, 0, c)

	assert.EqualError(t, err, "error retrieving events")
}

type eventWrapper struct {
	id    int
	event Event
}

type snapshot struct {
	sequenceNo int
	aggregate  string
	when       time.Time
}

func (s *snapshot) SequenceNo() int {
	return s.sequenceNo
}

func (s *snapshot) When() time.Time {
	return s.when
}

var _ Snapshot = &snapshot{}

func NewInMemoryStore() *InMemoryStore {
	return &InMemoryStore{roots: make(map[string][]ID), events: make(map[string][]eventWrapper)}
}

type InMemoryStore struct {
	events             map[string][]eventWrapper
	roots              map[string][]ID
	lastSnapshot       Snapshot
	GetError           error
	StoreError         error
	GetSnapshotError   error
	StoreSnapshotError error
}

func (s *InMemoryStore) Events(_ context.Context, fromId int, c chan Event, aggregateTypes ...Aggregate) error {
	if s.GetError != nil {
		return s.GetError
	}
	var result []EventWrapper

	if len(aggregateTypes) == 0 {
		for _, v := range s.events {
			for _, e := range v {
				if e.id >= fromId {
					result = append(result, EventWrapper{Event: e.event, AggregateSequenceNo: e.id})
				}
			}
		}
	} else {
		for _, aggregate := range aggregateTypes {
			if events, exists := s.events[aggregate.Identity().String()]; exists {
				for _, e := range events {
					if e.id > fromId {
						result = append(result, EventWrapper{Event: e.event, AggregateSequenceNo: e.id})
					}
				}
			}
		}
	}
	sort.SliceStable(result, func(i, j int) bool {
		return result[i].AggregateSequenceNo < result[j].AggregateSequenceNo
	})
	for _, e := range result {
		c <- e.Event
	}
	return nil
}

func (s *InMemoryStore) GetAggregateRoots(_ context.Context, aggregateType reflect.Type) ([]ID, error) {
	return s.roots[aggregateType.Name()], nil
}

func (s *InMemoryStore) RestoreSnapshot(_ context.Context, aggregate Aggregate) (Snapshot, error) {
	if s.GetSnapshotError != nil {
		return nil, s.GetSnapshotError
	}
	if s.lastSnapshot != nil {
		err := json.Unmarshal([]byte(s.lastSnapshot.(*snapshot).aggregate), aggregate)
		if err != nil {
			return nil, err
		}
	}
	return s.lastSnapshot, nil
}

func (s *InMemoryStore) StoreSnapshot(_ context.Context, aggregate Aggregate, sequenceNo int) error {
	if s.StoreSnapshotError != nil {
		return s.StoreSnapshotError
	}
	bytes, err := json.Marshal(aggregate)
	if err != nil {
		return err
	}
	s.lastSnapshot = &snapshot{
		sequenceNo: sequenceNo,
		aggregate:  string(bytes),
		when:       tstamp,
	}

	return nil
}

func (s *InMemoryStore) GetEvents(_ context.Context, aggregate Aggregate, sinceSnapshot Snapshot) ([]EventWrapper, error) {
	if s.GetError != nil {
		return nil, s.GetError
	}
	var result []EventWrapper
	if events, exists := s.events[aggregate.Identity().String()]; exists {
		for _, e := range events {
			if sinceSnapshot == nil || e.id > sinceSnapshot.SequenceNo() {
				result = append(result, EventWrapper{Event: e.event, AggregateSequenceNo: e.id})
			}
		}
	}
	return result, nil
}

func (s *InMemoryStore) StoreEvent(
	_ context.Context,
	aggregate Aggregate,
	event Event,
	sequenceNo int,
	newAggregate bool,
) error {
	if s.StoreError != nil {
		return s.StoreError
	}
	aType := reflect.TypeOf(aggregate).String()
	roots := s.roots[aType]
	if newAggregate && rootExist(roots, event.AggregateIdentity()) {
		return ErrAggregateAlreadyExists
	}
	if newAggregate {
		s.roots[aType] = append(s.roots[aType], event.AggregateIdentity())
	}
	var events []eventWrapper
	aggregateID := event.AggregateIdentity()
	s2 := aggregateID.String()
	if e, exists := s.events[s2]; exists {
		events = e
	}
	for _, e := range events {
		if e.id == sequenceNo {
			return ErrStaleAggregate
		}
	}
	events = append(events, eventWrapper{
		id:    sequenceNo,
		event: event,
	})
	s.events[aggregateID.String()] = events
	return nil
}

func (s *InMemoryStore) MaxGlobalSequenceNo(ctx context.Context) (int, error) {
	return 0, nil
}

var _ EventStore = &InMemoryStore{}

func rootExist(roots []ID, id ID) bool {
	for _, i := range roots {
		if i == id {
			return true
		}
	}
	return false
}

type InMemoryPublisher struct {
	events       []Event
	PublishError error
}

func (p *InMemoryPublisher) Publish(_ context.Context, event Event) error {
	if p.PublishError != nil {
		return p.PublishError
	}
	eventType := reflect.TypeOf(event).String()
	fmt.Printf("Publishing event of type '%s'\n", eventType)
	p.events = append(p.events, event)
	return nil
}

func (p *InMemoryPublisher) Stop(context.Context) error {
	return nil
}

var _ EventPublisher = &InMemoryPublisher{}

type CreateArea struct {
	Name string
}

func (c CreateArea) Validate(_ context.Context, aggregate Aggregate) error {
	if area, ok := aggregate.(*Area); ok {
		if area.ID != nil {
			return fmt.Errorf("id of aggregate is not empty: %+v", *area.ID)
		}
		return nil
	}
	return fmt.Errorf("aggregate provided is of wrong type: %+v", aggregate)
}

func (c CreateArea) Event(_ context.Context) Event {
	return &AreaCreated{Name: c.Name}
}

var _ Command = &CreateArea{}

type RenameArea struct {
	NewName string
}

func (r RenameArea) Validate(_ context.Context, aggregate Aggregate) error {
	if area, ok := aggregate.(*Area); ok {
		if area.Closed {
			return fmt.Errorf("area with id %s is closed and cannot be renamed", *area.ID)
		}
		return nil
	}
	return fmt.Errorf("aggregate provided is of wrong type: %+v", aggregate)
}

func (r RenameArea) Event(_ context.Context) Event {
	return &AreaRenamed{Name: r.NewName}
}

var _ Command = &RenameArea{}

type CloseArea struct {
	ID string
}

func (r CloseArea) Validate(_ context.Context, aggregate Aggregate) error {
	if area, ok := aggregate.(*Area); ok {
		if area.ID == nil || len(*area.ID) == 0 {
			return fmt.Errorf("unable to close area which have not been created")
		}
		return nil
	}
	return fmt.Errorf("aggregate provided is of wrong type: %+v", aggregate)
}

func (r CloseArea) Event(_ context.Context) Event {
	return &AreaClosed{}
}

var _ Command = &RemoveArea{}

type RemoveArea struct {
	ID string
}

func (r RemoveArea) Validate(_ context.Context, aggregate Aggregate) error {
	if area, ok := aggregate.(*Area); ok {
		if area.ID == nil || len(*area.ID) == 0 {
			return fmt.Errorf("unable to remove area which have not been created")
		}
		return nil
	}
	return fmt.Errorf("aggregate provided is of wrong type: %+v", aggregate)
}

func (r RemoveArea) Event(_ context.Context) Event {
	return &AreaRemoved{}
}

var _ Command = &RemoveArea{}

type UnexpectedCommand struct {
}

func (u UnexpectedCommand) Validate(_ context.Context, _ Aggregate) error {
	return nil
}

func (u UnexpectedCommand) Event(_ context.Context) Event {
	return &UnexpectedEvent{}
}

var _ Command = &UnexpectedCommand{}

type UnexpectedEvent struct {
	EventAggregateId
	EventTime
	EventGlobalSequence
}

var _ Event = &UnexpectedEvent{}

type AreaCreated struct {
	EventAggregateId
	EventTime
	EventGlobalSequence
	Name string
}

var _ Event = &AreaCreated{}

type AreaRenamed struct {
	Name string
	EventAggregateId
	EventTime
	EventGlobalSequence
}

var _ Event = &AreaRenamed{}

type AreaClosed struct {
	EventAggregateId
	EventTime
	EventGlobalSequence
	Name string
}

func (a *AreaClosed) EnrichFromAggregate(aggregate Aggregate) {
	switch ag := aggregate.(type) {
	case *Area:
		a.Name = ag.Name
	}
}

var _ EnrichableEvent = &AreaClosed{}

type AreaRemoved struct {
	EventAggregateId
	EventTime
	EventGlobalSequence
	Name string
}

func (a *AreaRemoved) EnrichFromAggregate(aggregate Aggregate) {
	switch ag := aggregate.(type) {
	case *Area:
		a.Name = ag.Name
	}
}

func (a *AreaRemoved) IsDeleteMarker() {
}

var _ EnrichableEvent = &AreaRemoved{}
var _ DeleteMarker = &AreaRemoved{}

type Area struct {
	BaseAggregate
	Name    string
	Closed  bool
	Removed bool
}

func (a *Area) Apply(event Event) error {
	switch e := event.(type) {
	case *AreaCreated:
		fmt.Printf("Applying AreaCreated: %+v\n", e)
		a.ID = &e.ID
		a.Name = e.Name
	case *AreaRenamed:
		fmt.Printf("Applying AreaRenamed: %+v\n", e)
		a.Name = e.Name
	case *AreaClosed:
		fmt.Printf("Applying AreaClosed: %+v\n", e)
		a.Closed = true
	case *AreaRemoved:
		fmt.Printf("Applying AreaRemoved: %+v\n", e)
		a.Removed = true
	default:
		return fmt.Errorf("unexpected event '%s' for Area with id: %v", reflect.TypeOf(event), a.Identity())
	}
	return nil
}

var _ Aggregate = &Area{}

type NilEventCommand struct{}

func (n NilEventCommand) Validate(_ context.Context, _ Aggregate) error {
	return nil
}

func (n NilEventCommand) Event(_ context.Context) Event {
	return nil
}

var _ Command = &NilEventCommand{}
