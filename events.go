/*
 * MIT License
 *
 * Copyright (c) 2022 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package eventsourced

// EnrichableEvent is a special event which triggers a call to EnrichFromAggregate from the
// CommandHandler after the event is applied and before it is published.
type EnrichableEvent interface {
	Event
	// EnrichFromAggregate is called by the CommandHandler after applying it to the aggregate
	// and allows the event to be enriched with additional information from the aggregate which
	// might be useful to provide richer events for other services to consume.
	EnrichFromAggregate(aggregate Aggregate)
}

// DeleteMarker is a special event used to mark an aggregate as deleted
// Actual deletion and cleanup is left to the EventStore to manage
type DeleteMarker interface {
	Event
	IsDeleteMarker()
}
