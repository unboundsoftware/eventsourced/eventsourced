# Changelog

All notable changes to this project will be documented in this file.

## [1.18.1] - 2025-03-10

### 🐛 Bug Fixes

- Update global sequence no of wrapped event

## [1.18.0] - 2025-03-09

### 🚀 Features

- Add Unwrap method to ExternalEvent and corresponding tests

### 🐛 Bug Fixes

- *(tests)* Correct spelling of function name in test file

## [1.17.0] - 2025-02-27

### 🚀 Features

- *(golangci)* Enable parallel runners and set timeout
- Redefine external event structure with generics

### ⚙️ Miscellaneous Tasks

- *(ci)* Update golangci-lint version to v1.64.4
- *(ci)* Update Go image to use amd64 architecture

## [1.16.0] - 2024-11-27

### 🚀 Features

- Add ExternalSource and ExternalContainer interfaces

### 🐛 Bug Fixes

- *(deps)* Update module github.com/stretchr/testify to v1.10.0

## [1.15.0] - 2024-10-05

### 🚀 Features

- Switch to UUIDv7

### 🐛 Bug Fixes

- *(deps)* Update module github.com/stretchr/testify to v1.9.0

### ⚙️ Miscellaneous Tasks

- Add release flow

## [1.14.1] - 2023-12-17

### 🐛 Bug Fixes

- Enrich event before saving

### ⚙️ Miscellaneous Tasks

- Use go 1.21.4
- Update version of Go and golangci-lint

## [1.14.0] - 2023-10-17

### 🚀 Features

- Add event sequence and MaxGlobalSequenceNo to EventStore

### ⚙️ Miscellaneous Tasks

- Change default branch to main
- Update to golang 1.20.1
- Update to golangci-lint 1.51.2
- Update to Go 1.20.3
- *(deps)* Update to Go 1.20.5
- Update Go and golangci-lint versions

## [1.13.0] - 2023-01-26

### 🚀 Features

- Add automatic refresh of stale aggregate
- [**breaking**] Rename GetSnapshot to RestoreSnapshot
- Remove unused When-func from Snapshot

## [1.12.0] - 2023-01-24

### 🚀 Features

- [**breaking**] Make command handler decide next sequenceNo of stored event

## [1.11.5] - 2023-01-22

### 🐛 Bug Fixes

- Remove useless Apply-func from BaseAggregate

## [1.11.4] - 2023-01-19

### 🐛 Bug Fixes

- Error expectation

### ⚙️ Miscellaneous Tasks

- Move enriching of events after apply and store

## [1.11.3] - 2022-11-21

### ⚙️ Miscellaneous Tasks

- Add more info to trace name and add trace around Apply

## [1.11.2] - 2022-11-15

### ⚙️ Miscellaneous Tasks

- Add context to allow span nesting

## [1.11.1] - 2022-11-11

### ⚙️ Miscellaneous Tasks

- Move error handling to option-funcs

## [1.11.0] - 2022-11-04

### 🚀 Features

- Add tracing

### ⚙️ Miscellaneous Tasks

- Add checks that publisher and traceHandler is not nil

## [1.10.1] - 2022-10-25

### ⚙️ Miscellaneous Tasks

- Remove Stop from EventPublisher

## [1.10.0] - 2022-10-11

### 🚀 Features

- [**breaking**] Add context to most methods

### 🐛 Bug Fixes

- Lint error

### ⚙️ Miscellaneous Tasks

- Replace golint with golangci-lint and run as separate step
- Add vulnerability-check

## [1.9.3] - 2022-08-18

### 🚀 Features

- Add support for enrichable events

## [1.9.2] - 2022-06-21

### 🚀 Features

- Add a couple of helper-funcs

## [1.9.1] - 2022-05-11

### 🚀 Features

- Recreate snapshots if missing

## [1.9.0] - 2022-04-07

### 🐛 Bug Fixes

- Implement Event-func in DeleteAggregate command

## [1.9.0-beta] - 2022-02-14

### 🚀 Features

- Allow aggregate to be marked as deleted

## [1.8.1] - 2022-01-28

### 🚀 Features

- Set aggregate identity during creation

### ⚙️ Miscellaneous Tasks

- Add dependabot config
- Remove dependabot-standalone
- Change to codecov binary instead of bash uploader

## [1.8.0] - 2020-12-04

### 🐛 Bug Fixes

- Add param to StoreEvent so EventStore knows if aggregate should be created

## [1.7.0] - 2020-12-03

### 🐛 Bug Fixes

- Move creation of aggregate into transaction when storing event

## [1.6.3] - 2020-11-25

### 🐛 Bug Fixes

- Add done-chan to fix failing test

### ⚙️ Miscellaneous Tasks

- Remove EventsFromEnd and range over chan instead
- Change sequenceNo to fromId instead
- Fix names in InMemoryStore

## [1.6.2] - 2020-11-18

### 🚀 Features

- Add error for EventStore to return when an aggregate already exists

## [1.6.0] - 2020-11-03

### 🚀 Features

- [**breaking**] Handle different aggregates with same id

### ⚙️ Miscellaneous Tasks

- Add codecov upload
- Change filename of coverage-profile
- Simplify pipeline
- Return specific error when no events or snapshots are found
- Update year in copyright

## [1.5.0] - 2020-03-24

### 🐛 Bug Fixes

- Unmarshal snapshot to aggregate instead

### ⚙️ Miscellaneous Tasks

- Cleanup

## [1.4.0] - 2019-12-19

### 🚀 Features

- Aggregate roots

### 🧪 Testing

- Used NewEventAggregateId in testcases

### ⚙️ Miscellaneous Tasks

- Comments
- Removed linitin for now

## [1.3.2] - 2019-11-19

### 🚀 Features

- Add sequence no to Events-func

## [1.3.1] - 2019-11-19

### 🐛 Bug Fixes

- Lint errors

## [1.3.0] - 2019-11-19

### 🚀 Features

- Make it more obvious that events are dealing with aggregate ids

## [1.2.4] - 2019-11-18

### 🚀 Features

- Add Events-func

## [1.2.3] - 2019-11-18

### 🐛 Bug Fixes

- Remove * from returned Event since it's an interface

## [1.2.2] - 2019-11-13

### 🐛 Bug Fixes

- Return error for missing aggregate

### ⚙️ Miscellaneous Tasks

- Fixed formatting

## [1.2.0] - 2019-10-29

### 🚀 Features

- Add context to Command.Validate and Command.Event and switch from builder-pattern to option funcs.

<!-- generated by git-cliff -->
