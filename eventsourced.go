// Copyright (c) 2020 Unbound Software Development Svenska AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package eventsourced

import (
	"context"
	"errors"
	"reflect"
	"time"
)

// Command contains the methods that all commands need to implement
type Command interface {
	// Validate allows for validation of the command before applying it
	Validate(ctx context.Context, aggregate Aggregate) error
	// Event returns the event to be applied to the Aggregate
	Event(ctx context.Context) Event
}

// ID is used for identifying an Event or an Aggregate
type ID string

// Id creates a pointer to ID
func Id(id ID) *ID {
	return &id
}

// IdFromString creates a pointer to ID from the provided string
func IdFromString(id string) *ID {
	res := ID(id)
	return &res
}

func (p *ID) String() string {
	if p == nil {
		return "<nil>"
	}
	return string(*p)
}

// Snapshot contains the methods that all snapshots need to implement
type Snapshot interface {
	SequenceNo() int
}

// Event contains the methods that all events need to implement
type Event interface {
	AggregateIdentity() ID
	SetAggregateIdentity(id ID)
	When() time.Time
	SetWhen(t time.Time)
	GlobalSequenceNo() int
	SetGlobalSequenceNo(seqNo int)
}

type BaseEvent struct {
	EventAggregateId
	EventTime
	EventGlobalSequence
}

// Aggregate contains the methods that all aggregates need to implement
type Aggregate interface {
	Identity() *ID
	// SetIdentity is used by eventsourced internally to set the aggregate ID when created and
	// should not be called elsewhere.
	SetIdentity(id ID)
	Apply(event Event) error
}

// EventPublisher contains the methods that all event publishers need to implement
type EventPublisher interface {
	Publish(ctx context.Context, event Event) error
}

// EventWrapper is a representation of an Event and it's aggregate sequence number
type EventWrapper struct {
	Event               Event
	AggregateSequenceNo int
}

// EventStore contains the methods that all event stores need to implement.
// An event store need to keep track of which aggregate roots exists, an ordered
// list of events in the order of addition (needs to keep a unique sequence
// of numeric ids of all events, referenced as global sequence no) as well as a list of snapshots of aggregates.
type EventStore interface {
	// RestoreSnapshot retrieves the latest snapshot for the provided aggregate,
	// should return nil if no snapshot is found.
	RestoreSnapshot(ctx context.Context, aggregate Aggregate) (Snapshot, error)
	// StoreSnapshot store a snapshot of the provided aggregate at the specified sequence number.
	StoreSnapshot(ctx context.Context, aggregate Aggregate, aggregateSequenceNo int) error
	// GetEvents retrieve all events for the provided aggregate since the provided snapshot,
	// retrieve all events if sinceSnapshot is nil.
	GetEvents(ctx context.Context, aggregate Aggregate, sinceSnapshot Snapshot) ([]EventWrapper, error)
	// StoreEvent store the provided event for the provided aggregate with aggregateSequenceNo.
	// Set newAggregate to true if this is the first event for the aggregate.
	StoreEvent(ctx context.Context, aggregate Aggregate, event Event, aggregateSequenceNo int, newAggregate bool) error
	// GetAggregateRoots retrieve the id's of all aggregate roots for the provided type.
	GetAggregateRoots(ctx context.Context, aggregateType reflect.Type) ([]ID, error)
	// Events retrieve all events since fromGlobalSequenceNo (the unique id of events regardless of which aggregate
	// the event is for) for the provided aggregate types and send each event to the provided channel.
	Events(ctx context.Context, fromGlobalSequenceNo int, c chan Event, aggregateTypes ...Aggregate) error
	// MaxGlobalSequenceNo retrieve the maximum global sequence no.
	MaxGlobalSequenceNo(ctx context.Context) (int, error)
}

// CommandHandler contains the methods that command handlers need to implement
type CommandHandler interface {
	Handle(ctx context.Context, command Command) (Event, error)
}

// IDGenerator is the definition of an aggregate ID generator
type IDGenerator func() string

// ErrNoAggregate is returned by NewHandler when no events or snapshots are found.
var ErrNoAggregate = errors.New("no aggregate with provided id and type was found")

// ErrStaleAggregate is returned by CommandHandler.Handle when another event has been applied
// since CommandHandler was initialized, i.e. the expected next sequence number is in use.
var ErrStaleAggregate = errors.New("the aggregate provided has been updated by someone else")

// ErrAggregateAlreadyExists is returned by EventStore.StoreEvent when creation of an already existing aggregate is tried.
var ErrAggregateAlreadyExists = errors.New("aggregate with provided id and type already exists")

// ErrIllegalSnapshotInterval is returned by NewHandler if a snapshot interval is not between 1 and math.MaxInt
var ErrIllegalSnapshotInterval = errors.New("snapshot interval must be larger at least 1")

// ErrNoPublisher is returned by NewHandler if publisher is set to nil.
var ErrNoPublisher = errors.New("publisher must be set to a non-nil value")

// ErrNoIDGenerator is returned by NewHandler if idGenerator is set to nil.
var ErrNoIDGenerator = errors.New("idGenerator must be set to a non-nil value")

// ErrNoTraceHandler is returned by NewHandler if traceHandler is set to nil.
var ErrNoTraceHandler = errors.New("traceHandler must be set to a non-nil value")
