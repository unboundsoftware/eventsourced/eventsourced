package eventsourced

import (
	"context"
)

// DeleteAggregate is a command that will mark the aggregate as deleted
type DeleteAggregate struct{}

func (d DeleteAggregate) Event(context.Context) Event {
	return &AggregateDeleted{}
}

func (d DeleteAggregate) Validate(context.Context, Aggregate) error {
	return nil
}

var _ Command = &DeleteAggregate{}

type AggregateDeleted struct {
	EventAggregateId
	EventTime
	EventGlobalSequence
}

func (a AggregateDeleted) IsDeleteMarker() {}

var _ Event = &AggregateDeleted{}

var _ DeleteMarker = &AggregateDeleted{}
