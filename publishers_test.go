// Copyright (c) 2020 Unbound Software Development Svenska AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package eventsourced

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestChannelPublisher_Publish(t *testing.T) {
	c := make(chan Event, 2)

	publisher := NewChannelPublisher(c)

	areaCreated := &AreaCreated{
		Name: "abc",
	}
	err := publisher.Publish(context.Background(), areaCreated)
	assert.NoError(t, err)

	ev := <-c
	assert.NotNil(t, ev)
	assert.Equal(t, areaCreated, ev)
}

func TestNoOpPublisher_Publish(t *testing.T) {
	areaCreated := &AreaCreated{
		Name: "abc",
	}
	err := (&noOpPublisher{}).Publish(context.Background(), areaCreated)
	assert.NoError(t, err)
}

func TestNoOpPublisher_Stop(t *testing.T) {
	err := (&noOpPublisher{}).Stop(context.Background())
	assert.NoError(t, err)
}
