/*
 * MIT License
 *
 * Copyright (c) 2022 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package eventsourced

import "context"

// Events fetches all events for the provided aggregate and runs the provided func on each in turn
func Events(ctx context.Context, aggregate Aggregate, store EventStore, f func(Event, int) error) error {
	events, err := store.GetEvents(ctx, aggregate, nil)
	if err != nil {
		return err
	}
	for _, e := range events {
		if err := f(e.Event, e.AggregateSequenceNo); err != nil {
			return err
		}
	}
	return nil
}

// EventsFrom fetches all events with an id greater than the provided id in order and adds them to the channel c
func EventsFrom(ctx context.Context, store EventStore, fromId int, c chan Event, aggregateTypes ...Aggregate) error {
	return store.Events(ctx, fromId, c, aggregateTypes...)
}
